require 'set'
class SimilarityMatrixFormation

  attr_accessor :appstack_to_be_deleted

  def self.initialize_appstack_to_be_deleted
    @appstack_to_be_deleted = []
  end

  def self.create_similarity_matrix(arrayofrequestsfrommachines)

    # arrayofrequestsfrommachines = [[1,4,5,6],[0,2,7,8,9],[6,3,8,9],[8,9]]

    #x = MultiSet.new([1,1,2,2,3,4,5,6])
    #y = MultiSet.new([1,3,5,6])

    #p x - y # [2,2,4]
    #p x & y # [1,3,5,6]
    #p x | y # [1,2,3,4,5,6]
    similarity_matrix = Array.new
    arrayofrequestsfrommachines.each_with_index do |requestarray1, index1|
      similarity_matrix_for_a_machine = Array.new
      arrayofrequestsfrommachines.each_with_index do |requestarray2, index2|
        x = Set.new([requestarray1])
        y = Set.new([requestarray2])
        similarity_matrix_for_a_machine << ((requestarray1 & requestarray2) == [] ? [] : (requestarray1 & requestarray2))
      end
      similarity_matrix << similarity_matrix_for_a_machine
    end

    similarity_matrix.each_with_index do |row_value,index1|
      row_value.each_with_index do |row_entity,index2|
        similarity_matrix[index1][index2] = [] if index1 > index2
      end
    end

    return similarity_matrix
  end

  def self.delete_row (similarity_matrix,index_of_the_row, minimum_value_assignment)
    @appstack_to_be_deleted << [similarity_matrix[index_of_the_row][index_of_the_row], minimum_value_assignment]if similarity_matrix[index_of_the_row][index_of_the_row] !=[]
    similarity_matrix.delete_at(index_of_the_row)
    similarity_matrix
  end

  def self.get_appstack_to_be_deleted
    @appstack_to_be_deleted
  end

end