require 'set'
class RevAlgorithmForOptimalStorage

  attr_accessor :indices_removed_on_the_fly
  attr_accessor :least_index_for_iteration
  attr_accessor :resulting_appstacks

  def initialize(users_request, max_number_of_assignments_per_machine, max_number_of_appstacks_per_assignment)
    @users_request, @max_number_of_assignments_per_machine, @max_number_of_appstacks_per_assignment =  users_request, max_number_of_assignments_per_machine, max_number_of_appstacks_per_assignment
    @indices_removed_on_the_fly = []
    @resulting_appstacks = []
  end

  def get_max_no_of_assignment_per_machine
    @max_number_of_assignments_per_machine
  end

  def put_max_no_of_assignments_machine max_number_of_assignments_per_machine
    @max_number_of_assignments_per_machine = max_number_of_assignments_per_machine
  end

  def get_minimum_indices similarity_matrix
    indices = []
    sorted_assignments_by_value = @max_number_of_assignments_per_machine.sort.uniq
    min_value =  sorted_assignments_by_value.min
    negative_values = (((-1)*(40))..(1)).to_a
    min_value_but_one = (sorted_assignments_by_value - (negative_values))
    min_assignment_value = 1000
    @max_number_of_assignments_per_machine.each_with_index do |assignment_value,index1|
      count = 0
      if (min_value_but_one & [assignment_value]) == [assignment_value]
        similarity_matrix[index1].each_with_index do |row_value, index3|
          if min_assignment_value >= assignment_value
            if row_value !=[] && index1<index3
              min_assignment_value = assignment_value
              count +=1
            end
          end
        end
      end

      if count!=0
        indices.push index1
      end

    end
    #p 'min -indices'
    #p indices
    indices
  end

  def get_index_with_common_largest_appstack (indices, similarity_matrix)

    max_count = 0
    wanted_index = -1
    similarity_matrix.each_with_index  do |row_values, index1|
      if (indices & [index1])!=[] && indices!=[]
        row_values.each_with_index do |row_value,index2|
          if (max_count < row_value.count && index1<index2)
            max_count = row_value.count
            wanted_index = index1
          end
        end
      end
    end
    wanted_index==-1? 0 : wanted_index
  end

  def get_largest_appstack_inside_maximum_number_of_appstack (index_of_row, row_of_the_similarity_matrix)
    appstack = []
    row_of_the_similarity_matrix.each_with_index do |row,index|
      if index_of_row < index
        appstack = row if appstack.count < row.count && row.count <= @max_number_of_appstacks_per_assignment
      end
    end
    #p '----------------------largest appstack inside maximum number of appstack constraint'
    #p appstack
    @resulting_appstacks << appstack
    appstack
  end

  def split_appstack_which_subsumes_it(appstack, similarity_matrix)
    similarity_matrix.each_with_index  do |row_values, index1|
      row_values.each_with_index do |row_value , index2|
        if index1 < index2
          if ( appstack & row_value ) == appstack
            row_value = row_value - appstack
            row_values[index2] = row_value
            @indices_removed_on_the_fly << [index1,appstack,row_value,index1+1,index2+1,'1']
          end
        end
      end
      similarity_matrix[index1] = row_values
    end
    similarity_matrix
  end

  def remove_apps_from_appstacks_subsumed_by_it(similarity_matrix, n, appstack, minimum_indices )
    combinations_appstacks_lenght_within_n =  (appstack.count).upto(appstack.count).flat_map { |n| appstack .combination(n).to_a }
    combinations_appstacks_lenght_within_n_reverse = combinations_appstacks_lenght_within_n.reverse
    combinations_appstacks_lenght_within_n_reverse.each_with_index do |combination , index1|
      similarity_matrix.each_with_index  do |row_values, index2|
        #if ([index2] & minimum_indices ).count!=0
        row_values.each_with_index do |row_value , index3|
          if index2 < index3 && row_value!=[]
            if (!((combination  & row_value).empty?) && ((combination  & row_value).count == combination.count - 1) || ((combination  & row_value).count == combination.count) || ((combination  & row_value).count == row_value.count))
              row_values[index3] = row_values[index3] - (combination  & row_value)
              @indices_removed_on_the_fly << [index2,index3]
            end
          end
        end
        similarity_matrix[index2] = row_values
      end
      #end
    end
    #p '@indices_removed_on_the_fly'
    #p @indices_removed_on_the_fly
    similarity_matrix
  end

  def remove_apps_from_appstacks_through_commoness(similarity_matrix, n, appstack, minimum_indices )
    count = 0
    similarity_matrix.each_with_index do |row_values,index1|
      row_values.each_with_index do |row_value,index2|
        if row_value & appstack !=[] && index1 == index2
          if check_for_removal(row_value,appstack,n)
            count += 1
            row_values[index2] = row_value - (row_value & appstack)
            @indices_removed_on_the_fly << [index1,index2]
          end
        end
      end
      if count!=0
        #@max_number_of_assignments_per_machine[index1]-=1
        #similarity_matrix[index1] = row_values
        count = 0
      end
    end
    similarity_matrix
  end

  def check_for_removal(array1,array2,number)
    if(array1.count <= array2.count)
      if array2.count - (array2 & array1).count <= number
        return true
      end
    end

    if(array2.count < array1.count)
      if (array1 - (array1 & array2)) == [] || (array2 - (array1 & array2) == [])
        return true
      end
      if array1.count - (array2 & array1).count <= number
        return true
      end
    end


    return false

  end

  def remove_apps_from_appstacks_through_commoness_diagonals(similarity_matrix, n, appstack, minimum_indices )
    count = 0
    similarity_matrix.each_with_index do |row_values,index1|
      if row_values[index1] & appstack !=[]
        if check_diagonal_for_removal(row_values[index1],appstack,n)
          row_values[index1] = row_values[index1] - (row_values[index1] & appstack)
          @max_number_of_assignments_per_machine[index1] -=1
        end
      end
    end
    similarity_matrix
  end

  def check_diagonal_for_removal(array_row_value,appstack_array,number)

    if (array_row_value - (array_row_value & appstack_array)) == [] || (appstack_array - (array_row_value & appstack_array) == [])
      return true
    end
    if (appstack_array - (appstack_array & array_row_value)).count <= number
      return true
    end

  end

  def reduce_weights_and_remove_appstacks_from_diagonals(similarity_matrix, appstack)
    similarity_matrix.each_with_index do |row_values,index1|
      if ([index1] & @indices_removed_on_the_fly.flatten.uniq) != []
        #p index1
        @max_number_of_assignments_per_machine[index1] -= 1
        #similarity_matrix[index1][index1] = similarity_matrix[index1][index1] - (similarity_matrix[index1][index1] & appstack)
      end
    end
    #p '@max_number_of_assignments_per_machine'
    #p @max_number_of_assignments_per_machine
    #p 'similarity'
    #p similarity_matrix
    @indices_removed_on_the_fly = []
    similarity_matrix
  end

  def get_indices_removed_on_the_fly
    @indices_removed_on_the_fly
  end

  def group_all_diagonal_elements_following_constraints (similarity_matrix)
    diagonal_groups = []
    similarity_matrix.each_with_index do |row_values,index1|
      if similarity_matrix[index1][index1] != []
        diagonal_groups << [similarity_matrix[index1][index1],@max_number_of_assignments_per_machine[index1]]
      end
    end
    diagonal_groups
  end

  def check_to_end_iteration(similarity_matrix)
    count1 = 0
    count2 = 0
    @max_number_of_assignments_per_machine.each_with_index do |assignment_value|
      if assignment_value>1
        count1+=1
      end
    end

    similarity_matrix.each_with_index do |row_values,index1|
      row_values.each_with_index do |row_value,index2|
        if (index1 < index2) && (@max_number_of_assignments_per_machine[index1] > 1)
          count2 +=1 if row_value != []
        end
      end
    end

    if count1 !=0 && count2 !=0
      return true
    end
    false
  end

  def sort_user_request_and_constraint (user_request,max_no_of_assignment_per_machine)
    i = 0
    j = 0
    a = @max_number_of_assignments_per_machine
    temp_user_id = 0
    while(i<max_no_of_assignment_per_machine.count)
      while(j<i)
        if a[i] < a[j]
          request_temp = user_request[i]
          index_temp = a[i]
          user_request[i] = user_request[j]
          a[i] = a[j]
          user_request[j]=request_temp
          a[j] = index_temp
        end
        j +=1
      end
      i+=1
      j =0
    end
    user_request
  end

  def get_diagonal_elements_of_similarity_matrix similarity_matrix
    diagonal_elements = []
    similarity_matrix.each_with_index do |value,index1|
      diagonal_elements << value[index1]
    end
    diagonal_elements
  end

  def get_absolute_appstacks semicomplete_appstacks
    fixed_appstacks = []
    amorphous_appstacks = []

    semicomplete_appstacks.each_with_index do |semicomplete_appstack,index|
      if semicomplete_appstack[1] == 1
        fixed_appstacks << semicomplete_appstack[0]
      else
        amorphous_appstacks << semicomplete_appstack[0]
      end
    end

    fixed_appstacks = fixed_appstacks.sort_by {|x| x.count}
    fixed_appstacks.reverse!
    semicomplete_appstacks.each_with_index do |semicomplete_appstack,index|
      if semicomplete_appstack[1] > 1
        fixed_appstacks.each_with_index do |fixed_appstack,index|
          if (fixed_appstack & semicomplete_appstack[0]) == fixed_appstack
            if (semicomplete_appstack[0] - fixed_appstack).count <= @max_number_of_appstacks_per_assignment
              semicomplete_appstack[0] = semicomplete_appstack[0] - fixed_appstack
              semicomplete_appstack[1] -= 1
            end
          end
        end
      end
    end

    semicomplete_appstacks.each_with_index do |semicomplete_appstack,count|
      if semicomplete_appstack[0] == []
        semicomplete_appstacks.delete_at(count)
      end
    end

    semicomplete_appstacks.each_with_index do |semicomplete_appstack,index|
      if (semicomplete_appstack[0].count) > @max_number_of_appstacks_per_assignment && semicomplete_appstack[0]!= []
        semicomplete_appstack[0] = semicomplete_appstack[0].each_slice(@max_number_of_appstacks_per_assignment).to_a
        semicomplete_appstack[1] -= (semicomplete_appstack[0].count - 1)
      end
    end
    semicomplete_appstacks
  end

  def convert_absolute_to_final absolute_appstacks
    final_appstacks = []
    count = 0
    absolute_appstacks.each_with_index do |absolute_appstack,index1|
      absolute_appstack[0].each_with_index do |aorn, index2|
        if aorn.class == Array
          final_appstacks << aorn
          count += 1
        end
      end
      if count == 0
        final_appstacks << absolute_appstack[0]
      end
      count = 0
    end
    final_appstacks
  end

end