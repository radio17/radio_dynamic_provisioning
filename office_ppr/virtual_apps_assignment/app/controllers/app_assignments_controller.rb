class AppAssignmentsController < ApplicationController
  def show

    max_number_of_appstacks_per_assignment = rand(7..15)#2
    number_of_machines = 1000
    #max_number_of_assignments_per_machine = [2,1,1,2,2,2,2]#[5,3,7,5,6,7,3] #Array.new(number_of_machines) {rand(2..7)} #[2,3,2,2,4] #[2,4,6,2,4,3]
    users_id = 1..number_of_machines
    #new_thing = max_number_of_assignments_per_machine.dup #[6,8,6,2,2,5]
    max_number_of_assignments_per_machine = []
    users_request =  Array.new(number_of_machines)
    #users_request.each_with_index { |request_values,index| (max_number_of_assignments_per_machine <<  (request_values.count.to_f/max_number_of_appstacks_per_assignment).ceil) }
    new_thing = max_number_of_assignments_per_machine.dup
    #old_users_request = [[9,8],[7,9,4,6,8,2],[3,8,9,2,4],[2,6,4,5,8],[6,3,4,2,5,9],[7,6]]
    invalid_requests = []
    users_request.each_with_index do |row_values,index1|
      new_array = Array.new(rand(1.30)) {rand(1..30)}.uniq!
      while new_array.nil? == true
        new_array = Array.new(rand(1..30)) {rand(1..30)}.uniq!
      end
      row_values = new_array
      users_request[index1] = row_values
      #[[1,4,5,6], [0,1,2,3,9], [4,5,7,8,9], [3,6,8,9],[0,2,7,8,9]]
    end
    old_users_request = users_request.dup
    sum =0
    max_number_of_appstacks_per_assignment = users_request.each {|request_values| sum+=(Math.sqrt(request_values.flatten.count))}
    ans = (sum.to_f/(users_request.count)).ceil
    max_number_of_appstacks_per_assignment = ((ans) + Math.sqrt(ans)).ceil
    users_request.each_with_index { |request_values,index| (max_number_of_assignments_per_machine <<  (request_values.count.to_f/max_number_of_appstacks_per_assignment).ceil+1) }
    new_thing = max_number_of_assignments_per_machine.dup
    similarity = SimilarityMatrixFormation.create_similarity_matrix(users_request)
    revalgoentities = RevAlgorithmForOptimalStorage.new(users_request, max_number_of_assignments_per_machine, max_number_of_appstacks_per_assignment)
    msg = ""
    @resulting_appstacks = []
    SimilarityMatrixFormation.initialize_appstack_to_be_deleted
    #while revalgoentities.check_to_end_iteration(similarity)
    while (similarity.count > 1)
      users_request=revalgoentities.sort_user_request_and_constraint(users_request,max_number_of_assignments_per_machine)
      max_number_of_assignments_per_machine = revalgoentities.get_max_no_of_assignment_per_machine
      similarity = SimilarityMatrixFormation.create_similarity_matrix(users_request)
      indices_for_delete = []
      similarity.each_with_index do |row_values,index1|
        if (similarity[index1][index1].count > max_number_of_assignments_per_machine[index1] * max_number_of_appstacks_per_assignment) || (max_number_of_assignments_per_machine[index1] == 1 || (users_request[index1] == []))
          indices_for_delete << index1
        end
      end

      indices_for_delete.each_with_index do |index_for_delete, index|
        similarity = SimilarityMatrixFormation.delete_row(similarity,(index_for_delete-index),max_number_of_assignments_per_machine[index_for_delete-index])
        max_number_of_assignments_per_machine.delete_at(index_for_delete - index)
        revalgoentities.put_max_no_of_assignments_machine max_number_of_assignments_per_machine
        users_request.delete_at(index_for_delete - index)
        similarity = SimilarityMatrixFormation.create_similarity_matrix(users_request)
      end
      invalid_requests = (SimilarityMatrixFormation.get_appstack_to_be_deleted)

      new_similarity = similarity

      p 'similarity'
      p similarity

      p 'max_number_of_'

      minimum_indices = revalgoentities.get_minimum_indices similarity
      index_of_common_largest_appstack = revalgoentities.get_index_with_common_largest_appstack minimum_indices , similarity

      largest_appstack_inside_max = []
      minimum_indices.each_with_index do |minimum_index,index|
        appstack_obtained = revalgoentities.get_largest_appstack_inside_maximum_number_of_appstack(minimum_index,similarity[minimum_index])
        if appstack_obtained!=[] && largest_appstack_inside_max.count <= appstack_obtained.count
          largest_appstack_inside_max = appstack_obtained
        end
      end
      if largest_appstack_inside_max!=[]
        @resulting_appstacks << [largest_appstack_inside_max,1]
      end

      # largest_appstack_inside_max = revalgoentities.get_largest_appstack_inside_maximum_number_of_appstack(index_of_common_largest_appstack,similarity[index_of_common_largest_appstack])
      if largest_appstack_inside_max == []
        minimum_indices.each_with_index do |minimum_index_for_delete, index|
          similarity = SimilarityMatrixFormation.delete_row(similarity,(minimum_index_for_delete-index), max_number_of_assignments_per_machine[minimum_index_for_delete - index])
          max_number_of_assignments_per_machine.delete_at(minimum_index_for_delete - index)
          revalgoentities.put_max_no_of_assignments_machine max_number_of_assignments_per_machine
          users_request.delete_at(minimum_index_for_delete - index)
          similarity = SimilarityMatrixFormation.create_similarity_matrix(users_request)
        end
        invalid_requests = (SimilarityMatrixFormation.get_appstack_to_be_deleted)
        if !revalgoentities.check_to_end_iteration(similarity)
          break
        end
        next
      end

      #similarity = revalgoentities.split_appstack_which_subsumes_it largest_appstack_inside_max, similarity
      #similarity = revalgoentities.remove_apps_from_appstacks_subsumed_by_it similarity,1,largest_appstack_inside_max,minimum_indices
      # similarity = revalgoentities.remove_apps_from_appstacks_through_commoness similarity,1,largest_appstack_inside_max,minimum_indices
      similarity = revalgoentities.remove_apps_from_appstacks_through_commoness_diagonals similarity,1,largest_appstack_inside_max,minimum_indices
      max_number_of_assignments_per_machine = revalgoentities.get_max_no_of_assignment_per_machine
      #similarity = revalgoentities.reduce_weights_and_remove_appstacks_from_diagonals similarity,largest_appstack_inside_max
      users_request = revalgoentities.get_diagonal_elements_of_similarity_matrix similarity
      max_number_of_assignments_per_machine = revalgoentities.get_max_no_of_assignment_per_machine
    end

    revalgoentities.put_max_no_of_assignments_machine max_number_of_assignments_per_machine
    diagonal_elements = revalgoentities.group_all_diagonal_elements_following_constraints similarity
    diagonal_elements.each_with_index do |diagonal_element,index|
      if (diagonal_element[0].count/diagonal_element[1]) > max_number_of_appstacks_per_assignment
        invalid_requests << diagonal_element if diagonal_element[0] != []
      else
        @resulting_appstacks << diagonal_element if diagonal_element[0] != []
      end
    end

    ra = @resulting_appstacks.dup
    ir = invalid_requests.dup

    can_be_merged_appstacks = []
    cannot_be_merged_appstacks = []
    invalid_requests.each_with_index do |invalid_request,index|
      if (invalid_request[0].count) <= max_number_of_appstacks_per_assignment*invalid_request[1]
        can_be_merged_appstacks << invalid_request
      else
        cannot_be_merged_appstacks << invalid_request
      end
    end

    joint_appstacks = @resulting_appstacks.concat(can_be_merged_appstacks)
    joint_appstacks.each do |joint_appstack|
      joint_appstack[0].sort!
    end
    absolute_appstacks = revalgoentities.get_absolute_appstacks(joint_appstacks)
    final_appstacks = revalgoentities.convert_absolute_to_final(absolute_appstacks)

    #render :json =>  ['user_request', users_request, 'new_similarity' ,new_similarity,'completed_similarity', similarity,'max_no_assign_per_machine', new_thing,'max_no_appstack_per_assignment', max_number_of_appstacks_per_assignment]
    render :json =>  ['appstacks required to be attached for users', old_users_request, 'Maximum number of attachments for all machines ' ,new_thing,'max_no_application_per_attachment', max_number_of_appstacks_per_assignment,'invalid_request', cannot_be_merged_appstacks,'final_appstacks',final_appstacks.uniq]
  end
end